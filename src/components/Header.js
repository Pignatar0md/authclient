import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './HeaderStyle.css';

class Header extends React.Component {

  renderLinks() {
    if (this.props.authenticated) {
      return (
        <>
          <Link to="/feature">Feature</Link>
          <Link to="/signout">Sign out</Link>
        </>
      );
    } else {
      return (
        <>
          <Link to="/signup">Sign up</Link>
          <Link to="/signin">Sign in</Link>
        </>
      );
    }
  }

  render() {
    return (
      <div className="header">
        <Link to="/">Redux auth</Link>
        {this.renderLinks()}
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return { authenticated: auth.authenticated };
}
export default connect(mapStateToProps)(Header);