import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { signUp } from '../../actions';

class Signup extends React.Component {

  onSubmit = formProps => {
    this.props.signUp(formProps, () => {
      this.props.history.push('/feature');
    });
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <fieldset>
            <label>Email</label>
            <Field
              name="email"
              type="text"
              component="input"
              autoComplete="none"
            />
          </fieldset>
          <fieldset>
            <label>Password</label>
            <Field
              name="password"
              type="password"
              component="input"
              autoComplete="none"
            />
        </fieldset>
        <div>{this.props.errorMessage}</div>
        <button>Sign Up!</button>
      </form>
    )
  }
}

const mapStateToProps = ({ auth }) => {
  return { errorMessage: auth.errorMessage };
}

export default compose(
  connect(mapStateToProps, { signUp }),
  reduxForm({ form: 'signup' })
)(Signup);
// const wrapped = reduxForm({
//   form: 'signup'
// })(Signup);
// export default connect(mapStateToProps, { signUp })(wrapped);